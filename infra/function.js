function handler(event) {
    var request = event.request;
    var uri = request.uri;

    // this will let /foo resolve to /foo/index.html
    if (!uri.includes('.') && !uri.endsWith('/')) {
        uri += '/';
    }

    // this will let /foo/ resolve to /foo/index.html
    if (uri.endsWith('/')) {
        uri += 'index.html';
    }

    request.uri = uri;

    return request;
}
