import * as pulumi from '@pulumi/pulumi';

interface InfraConfig {
    domains: string[];
}

const pulumiConfig = new pulumi.Config();
const config = pulumiConfig.requireObject<InfraConfig>('config');
export default config;
