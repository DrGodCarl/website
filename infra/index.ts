import * as aws from "@pulumi/aws";
import * as pulumi from "@pulumi/pulumi";
import * as fs from "fs";
import config from "./config";

const eastRegion = new aws.Provider("east", {
  profile: aws.config.profile,
  region: "us-east-1", // ACM certificate must be in the us-east-1 region.
});

const ttl = 60 * 10;

const createCertificate = async (domain: string) => {
  const certificateConfig: aws.acm.CertificateArgs = {
    domainName: domain,
    validationMethod: "DNS",
    // subjectAlternativeNames: [`www.${domain}`],
  };

  const certificate = new aws.acm.Certificate(
    `${domain}-certificate`,
    certificateConfig,
    {
      provider: eastRegion,
    }
  );

  const { zoneId: hostedZoneId } = await aws.route53.getZone(
    { name: domain },
    { async: true }
  );

  // Validate the certificate.
  const certificateValidation = new aws.acm.CertificateValidation(
    `${domain}-certificate-validation`,
    {
      certificateArn: certificate.arn,
    },
    { provider: eastRegion }
  );

  return certificateValidation;
};

const createEdgeLambda = () => {
  return new aws.cloudfront.Function("website-edge-lambda", {
    runtime: "cloudfront-js-1.0",
    comment: "Function for mapping from / to /index.html",
    publish: true,
    code: fs.readFileSync(`./function.js`, "utf8"),
  });
};

const createCDN = async ({
  contentBucket,
  domain,
}: {
  contentBucket: aws.s3.BucketV2;
  domain: string;
}) => {
  // Use Origin Access Control to access the private s3 bucket.
  const originAccessControl = new aws.cloudfront.OriginAccessControl(
    `${domain}-origin-access-control`,
    {
      originAccessControlOriginType: "s3",
      signingBehavior: "always",
      signingProtocol: "sigv4",
    }
  );

  const distributionAliases = [domain];
  const cert = await createCertificate(domain);

  const lambda = createEdgeLambda();

  const distributionArgs: aws.cloudfront.DistributionArgs = {
    enabled: true,
    // Alternate aliases the CloudFront distribution can be reached at, in addition to https://xxxx.cloudfront.net.
    aliases: distributionAliases,

    // We only specify one origin for this distribution, the S3 content bucket.
    origins: [
      {
        domainName: contentBucket.bucketRegionalDomainName,
        originAccessControlId: originAccessControl.id,
        originId: contentBucket.arn,
      },
    ],

    defaultRootObject: "index.html",

    // A CloudFront distribution can configure different cache behaviors based on the request path.
    // Here we just specify a single, default cache behavior which is just read-only requests to S3.
    defaultCacheBehavior: {
      targetOriginId: contentBucket.arn,

      viewerProtocolPolicy: "redirect-to-https",
      allowedMethods: ["GET", "HEAD", "OPTIONS"],
      cachedMethods: ["GET", "HEAD", "OPTIONS"],

      forwardedValues: {
        cookies: { forward: "none" },
        queryString: false,
      },

      minTtl: 0,
      defaultTtl: ttl,
      maxTtl: ttl,

      compress: true,
    },

    orderedCacheBehaviors: [
      {
        pathPattern: "/*",
        allowedMethods: ["GET", "HEAD", "OPTIONS"],
        cachedMethods: ["GET", "HEAD", "OPTIONS"],
        targetOriginId: contentBucket.arn,
        forwardedValues: {
          queryString: false,
          cookies: {
            forward: "none",
          },
        },
        viewerProtocolPolicy: "redirect-to-https",
        functionAssociations: [
          {
            eventType: "viewer-request", // can be viewer-request or viewer-response
            functionArn: lambda.arn,
          },
        ],
        minTtl: 0,
        defaultTtl: ttl,
        maxTtl: 31536000,
      },
    ],

    priceClass: "PriceClass_100",

    // If S3 is 403ing, just kick 'em back to root.
    customErrorResponses: [
      { errorCode: 403, responseCode: 200, responsePagePath: "/index.html" },
      { errorCode: 404, responseCode: 200, responsePagePath: "/index.html" },
    ],

    restrictions: {
      geoRestriction: {
        restrictionType: "none",
      },
    },

    viewerCertificate: {
      acmCertificateArn: cert.certificateArn, // Per AWS, ACM certificate must be in the us-east-1 region.
      sslSupportMethod: "sni-only",
    },
  };
  return new aws.cloudfront.Distribution(`${domain}-cdn`, distributionArgs);
};

// Creates a new Route53 DNS record pointing the domain to the CloudFront distribution.
const createAliasRecords = async ({
  domain,
  distribution,
}: {
  domain: string;
  distribution: aws.cloudfront.Distribution;
}) => {
  const { zoneId: hostedZoneId } = await aws.route53.getZone(
    { name: domain },
    { async: true }
  );

  new aws.route53.Record(`${domain}-root-alias`, {
    name: domain,
    zoneId: hostedZoneId,
    type: "A",
    aliases: [
      {
        name: distribution.domainName,
        zoneId: distribution.hostedZoneId,
        evaluateTargetHealth: true,
      },
    ],
  });

  //   new aws.route53.Record(`${domain}-www-alias`, {
  //     name: `www.${domain}`,
  //     zoneId: hostedZoneId,
  //     type: "A",
  //     aliases: [
  //       {
  //         name: distribution.domainName,
  //         zoneId: distribution.hostedZoneId,
  //         evaluateTargetHealth: true,
  //       },
  //     ],
  //   });
};

export = async () => {
  const domains = config.domains;
  // TODO - support multiple domains
  const domain = domains[0];
  const siteBucket = new aws.s3.BucketV2("website-bucket");
  const bucketWebsiteConfig = new aws.s3.BucketWebsiteConfigurationV2(
    `website-config`,
    {
      bucket: siteBucket.bucket,
      errorDocument: { key: "404.html" },
      indexDocument: { suffix: "index.html" },
    }
  );

  // Create an S3 Bucket Policy to allow public read of all objects in bucket
  // This reusable function can be pulled out into its own module
  const bucketPolicyDoc = aws.iam.getPolicyDocumentOutput({
    statements: [
      {
        principals: [
          {
            type: "Service",
            identifiers: ["cloudfront.amazonaws.com"],
          },
        ],
        effect: "Allow",
        actions: ["s3:GetObject", "s3:GetObjectVersion"],
        resources: [siteBucket.arn, pulumi.interpolate`${siteBucket.arn}/*`],
      },
    ],
  });

  // Set the access policy for the bucket so all objects are readable
  let bucketPolicy = new aws.s3.BucketPolicy("website-bucket-policy", {
    bucket: siteBucket.bucket,
    policy: bucketPolicyDoc.apply((doc) => doc.json),
  });

  const distribution = await createCDN({
    contentBucket: siteBucket,
    domain,
  });
  await createAliasRecords({ domain, distribution });

  return {
    bucketName: siteBucket.id,
  };
};
