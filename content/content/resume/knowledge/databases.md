+++
weight = 40

[extra]
name = "Databases/Data Sources"
values = [
  "PostgreSQL",
  "MySQL",
  "MSSQL",
  "Neptune",
  "DB2",
  "Oracle",
  "Elasticsearch",
  "DynamoDB",
  "Redis"
]
display = true
+++