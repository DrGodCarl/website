+++
weight = 20

[extra]
name = "Java/JVM"
values = [
  "Gradle",
  "JUnit",
  "Spring 5",
  "Spring Boot 2.0+",
  "Spring Web",
  "Spring Batch",
  "Spring Security",
  "Spring Data JPA",
  "Axon",
  "Flyway",
  "Kotest",
  "MockK",
  "Mockito",
  "Lombok"
]
display = true
+++