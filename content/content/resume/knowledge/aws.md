+++
weight = 11

[extra]
name = "AWS"
values = [
  "S3",
  "ECS",
  "RDS",
  "EC2",
  "Fargate",
  "ECR",
  "CloudWatch",
  "Lambda",
  "SQS",
  "SNS",
  "Neptune",
  "Route 53",
  "CloudFront",
  "Elasticsearch Service",
  "AmazonMQ",
  "AppSync",
  "Cognito"
]
display = true
+++