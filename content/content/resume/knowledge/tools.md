+++
weight = 30

[extra]
name = "Tools"
values = [
  "Git",
  "IntelliJ",
  "Android Studio",
  "Xcode",
  "PyCharm",
  "VS Code",
  "DataGrip",
  "Postman",
  "Jira",
  "Trello",
  "Kanban Flow",
  "Asana",
  "Notion",
  "Confluence",
  "MediaWiki",
  "GitLab",
  "BitBucket",
  "GitHub",
  "Slack"
]
display = true
+++