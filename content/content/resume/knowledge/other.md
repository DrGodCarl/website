+++
# always last
weight = 1_000

[extra]
name = "The Rest"
values = [
  "Pulumi",
  "React",
  "React Native",
  "Flutter",
  "Hasura",
  "Agile",
  "Event Sourcing",
  "RESTful Services",
  "Dependency Injection",
  "Test Driven Development"
]
display = true
+++