+++
weight = 10

[extra]
name = "Languages"
values = [
  "Kotlin",
  "Java",
  "Python",
  "Typescript",
  "JavaScript",
  "Rust",
  "C#",
  "Swift",
  "Objective-C",
  "Dart",
  "Groovy"
]
display = true
+++