+++
weight = 3

[extra]
name = "Innovative Computer Software"
tasks = [
  "Began implementation on an iOS app for Great Clips that consumed a RESTful web service.",
  "Contributed to the Great Clips app for Android by updating the Google Maps integration."
]
display = true

[[extra.positions]]
title = "App Developer"
start = "June 2012"
end = "Oct 2013"
+++