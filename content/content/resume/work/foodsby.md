+++
weight = 7

[extra]
name = "Foodsby"
tasks = [
  #"Planned and contributed to a gradual migration to microservices.",
  #"Developed new relationship system using AWS's graph database, Neptune.",
  "Led effort to transition to an event-driven architecture using ActiveMQ for intra- and inter-service asynchronous communication.",
  #"Designed, built, and maintained robust APIs with consideration for future functionality using Spring Boot.",
  "Designed, built, and maintained a scalable system to support user-based customizations in the tens of thousands of emails that get sent daily.",
  "Planned and executed integrations with third-party email and tax filing providers.",
  "Led effort to introduce event sourcing using Axon for better historical tracking.",
  "Maintained a legacy ASP.NET monolith and Groovy microservices after introducing Kotlin, which became the primary language for our microservices.",
  #"Mentored more junior members of the engineering department.",
  #"Built and maintained shared libraries used across multiple teams."
]
display = true

[[extra.positions]]
title = "Staff Software Engineer"
start = "May 2018"
end = "July 2020"
+++