+++
weight = 6

[extra]
name = "Boothcast"
tasks = [
  "Co-founded a company for people to live-stream audio commentary over a sporting event.",
  "Led technical decision making and technical work, performed tech support for streamers, managed streamer schedules, and managed contractors.",
  "Built a backend in Spring Boot and Kotlin that supported authentication, tipping, and scheduled streams.",
  "Built and managed AWS infrastructure to host the backend using Fargate, RDS, S3, EC2, Route53, CloudFront, and others.",
  #"Developed, with the help of a contractor, chat functionality using AWS's AppSync, Dynamo, and Cognito.",
  #"Made and maintained CI pipelines to deploy the backend to Fargate and the websites to S3.",
  "Built a cross-platform mobile application using Flutter with audio streaming, in-app purchases, and chat as well as a web dashboard for streamers.",
  #"Integrated with and managed Wowza Streaming Engine using their AMI.",
  #"Created a streamer dashboard for monitoring tips and chat while streaming in React."
]
display = true

[[extra.positions]]
title = "Co-founder, CTO"
start = "April 2018"
end = "June 2020"
+++