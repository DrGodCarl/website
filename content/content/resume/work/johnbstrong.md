+++
weight = 2

[extra]
name = "John B. Strong"
tasks = [
  "Used Google App Engine and Google Maps API to develop a web app.",
  "Contributed to the Python RESTful backend in conjunction with my partner.",
  "Wrote the frontend in JavaScript partially via CoffeeScript, CSS3, and HTML5."
]
display = false

[[extra.positions]]
title = "Web App Developer"
start = "Dec 2011"
end = "May 2012"
+++