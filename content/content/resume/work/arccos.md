+++
weight = 8

[extra]
name = "Arccos Golf"
tasks = [
    "Built a scalable event-based platform that processes thousands of pieces of data collected during the course of a game of golf for thousands of people every day.",
    "Integrated with ML models made by the data science team to implement a \"smart distances\" formula that determines how far a player should expect to hit with each of their clubs.",
    "Migrated from ECS to Serverless in an effort to improve performance, scalability, and reliability for core systems resulting in substantially improved uptime."
]
display = true

[[extra.positions]]
title = "Software Architect"
start = "Aug 2020"
end = "May 2022"
+++