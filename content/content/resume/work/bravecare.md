+++
weight = 9

[extra]
name = "Brave Care"
tasks = [
    "Migrated authentication from Auth0 to Cognito with minimal interruption to users and substantial cost-savings to the company.",
    "Created internal tooling to aid in local development, specifically around Lambda's invoked directly from AWS (e.g. Cognito).",
    "Redesigned and rewrote the scheduling algorithms with reliability and flexibility in mind."
]
display = true

[[extra.positions]]
title = "Senior Backend Engineer III"
start = "May 2022"
end = "May 2023"
+++