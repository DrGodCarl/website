+++
weight = 4

[extra]
name = "Code42"
tasks = [
  "Invented method of detecting browser file uploads by leveraging existing technology.",
  "Assumed all feature and maintenance work of core application engine which was responsible for generating and receiving file change events within a peer-to-peer network.",
  "Optimized macOS and iOS endpoint engines using Apple's performance monitoring software.",
  #"Created automated tools for testing critical database migration upgrade path for iOS and macOS.",
  #"Led an effort to improve and test customer-facing Python scripts for Splunk data analysis.",
  "Refactored particularly bug-prone and difficult-to-debug areas of the legacy Java file storage engine.",
  #"Handled performance testing of server with a new networking stack."
]
display = true

[[extra.positions]]
title = "Jr. iOS / OS X Developer"
start = "Oct 2013"
end = "April 2015"

[[extra.positions]]
title = "Software Engineer"
start = "April 2015"
end = "March 2016"
+++