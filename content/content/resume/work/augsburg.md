+++
weight = 1

[extra]
name = "Augsburg College"
tasks = [
  "Analyzed space weather data after organizing it in a meaningful way.",
  "Designed and implemented software for marking useful areas of data for analysis.",
  "Led a redesign of the space physics research web page."
]
display = false

[[extra.positions]]
title = "Space Physics Research Assistant"
start = "May 2010"
end = "Sept 2010"
+++