+++
weight = 5

[extra]
name = "Daugherty Business Solutions"
tasks = [
  "Created a prescription drug monitoring program with microservice architecture using Spring Boot, Knockout.js, and Postgres.",
  #"Developed a robust API for dynamically querying the database based on many fields.",
  #"Created similarly robust and dynamic display tables to allow for data restrictions.",
  "Maintained and added features to a legacy codebase written in Java using the GWT UI framework.",
  "Created small cross-platform mobile app in Xamarin which sparked the client's interest in pursuing further mobile work.",
  "Built a prototype of product catalog with searchable documents and product categorization using Vue, Spring Boot, Spring Batch, Elasticsearch, and Postgres.",
  #"Led a team updating and maintaining multiple legacy applications written in Java using the Swing UI framework.",
  "Managed multiple other engineers and aided them in personal and professional growth."
]
display = true

[[extra.positions]]
title = "Software Engineer II / Consultant"
start = "March 2016"
end = "March 2017"

[[extra.positions]]
title = "Software Engineer III / Consultant"
start = "March 2017"
end = "August 2017"

[[extra.positions]]
title = "Software Engineer III /  Senior Consultant"
start = "August 2017"
end = "May 2018"
+++