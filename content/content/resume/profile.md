+++
[extra]
name="Carl D. Benson"
about="""
A decade of experience, a desire to define and constrain problems,
and a love of building systems to solve those problems.
"""
avatar="https://s.gravatar.com/avatar/d67bc4f69b99b7b8bd0125a2fc28c57e"
+++