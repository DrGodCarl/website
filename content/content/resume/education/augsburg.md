+++
weight = 1

[extra]
name = "Augsburg College"
start = "Sept 2008"
end = "May 2012"
results = [
  "Bachelors of Science in Physics",
  "Bachelors of Science in Mathematics",
  "Physics Honor Society - Sigma Pi Sigma"
]
+++